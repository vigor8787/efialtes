/**
 * @author vigor8787
 */

'use strict'

App.controller("documentListViewCtrl",[
		'$scope',
		'$http',
		'$modal',		
		'documentListService',
		'categoryService',
		function($scope,$http,$modal,documentListService,categoryService){		  
	
	var self = this;
	
	//결재양식 Data
	self.document = {
			name : '',
			categoryId:'',
			tags:[],		
			version : '',
			html : '',
			htmlWithComponent : '',
			description: ''			
		};
	
	self.selectedCategoryId='';
	
	
	//Category List Select Call in Service
	self.selectDocuments = function(document) {
		var document_tag_result = [];
		angular.forEach(self.document.tags, function(value, key) {	          				  
			document_tag_result.push(value.name);	          				  	          				 
		});
			
		self.document.tags = document_tag_result;
		
		$scope.documents = []; //초기화
		documentListService.selectDocuments(document).then(
				function(response) {
					for(var i=0; i < response.length ; i++) {
						//$scope.documents.push({ name: response[i].name, thumbNailImage: response[i].thumbNailImage});
						$scope.documents.push({ id: response[i].id, name: response[i].name, thumbNailImage: 'http://localhost:9000/getImage?path='+response[i].thumbNailImage});
					}					
					self.dataSetInList();
				});
	};
	
	self.dataSetInList = function() {
	    $scope.source = new kendo.data.DataSource({
	        data: $scope.documents ,
	        pageSize: 8
	    });
	};
	
	
	
	
	
	$scope.documents = [];
	$scope.tags = [];
	
	$scope.init = function() {
		self.document.categoryId = 'root';
		self.selectDocuments(self.document);
	};
		
	$scope.selectDocuments = function(categoryId) {
		self.selectedCategoryId = categoryId;		
		self.document.categoryId = categoryId;
		self.selectDocuments(self.document);
	};
	
	$scope.updateTag = function() {				
		self.selectDocuments(self.document);
	};
	
	
	
		
	//구현예정
    $scope.select = function() {
    	alert('상세조회');
    }        	
    
    //Tag loading Event
    $scope.loadTags = function(query) {
    	//return $http.get('/tags?query=' + query);
    }
        
	//양식 추가 팝업
	$scope.createDocument = function() {
		console.log('opening popup');
		var modalInstance = $modal.open({
			templateUrl: '/resources/template/documentRegister.html',
			controller: 'documentRegisterController as dcRegisterCtrl',
			size:'lg'				
		});		
		
		modalInstance.result.then(function() {		
			//alert('닫힐때');
		});			
	}	
	
	
	//양식 클릭
	$scope.imageClick = function(ClickedDocumentId) {
		alert(ClickedDocumentId);
	};
	
	}
]);





App.controller('documentRegisterController', [
	          		'$scope',          
	          		'$modalInstance',	          		
	          		'documentService',
	          		'categoryService',
	          		function($scope,$modalInstance,documentService,categoryService) {
	          			var self = this;
	          		          		          
	          			//결재양식 model
	          			self.document = {
	          				name : '',
	          				categoryId: '',
	          				tags:[],
	          				version : '',
	          				html : '',
	          				htmlWithComponent : '',
	          				thumNailImage: '',
	          				description: '',
	          				useCount:''
	          			};	          			
	          			//$scope.documents = [];
	          			
	          			
	          			//카테고리 model
	          			self.category = {
	          					categoryId : '',
	          					categoryName:''		
	          			};
	          			$scope.categories = [];      	
	          			
	          			self.categoryChange = function() {
	          				self.category.categoryId = $scope.categorySelected;
	          				//self.category.categoryName = $scope.category.categoryName;	          				
	          			};
	          			
	          			
	          			//결재양식 추가 화면 open Event
	          			$modalInstance.opened.then(function() {
	          				//등록된 카테고리 항목 조회
	          				self.selectCategories();
	          			});
		          				  
	        			//Category List Select Call in Service
	        			self.selectCategories = function() {
	        				categoryService.selectCategories().then(
	        						function(response) {
	        							//$scope.documents.push(response);							
	        							for(var i=0; i < response.length ; i++) {
	        								$scope.categories.push({ categoryId: response[i].id, categoryName: response[i].name });
	        							}
	        						});
	        			};
	          			
	          			self.createDocument = function(document) {
	          				document.categoryId = document.categoryId.categoryId; 	          					          				
	          				
	          				var document_tag_result = [];
	          				angular.forEach(document.tags, function(value, key) {	          				  
	          				  document_tag_result.push(value.name);	          				  	          				 
	          				});
	          				
	          				document.tags = document_tag_result;
	          					
	          				documentService.createDocument(document).then(
	          						console.log('usuario creado'), 
	          						function(errResponse) {
	          							console.error('Error while creating Document');
	          						});
	          				//$modalInstance.close();
	          			};
	          			
	          			self.close = function() {
          					$modalInstance.close();
	          			}
	
	          			self.submit = function() {
	          				console.log('Saving new document', self.document);
	          				self.createDocument(self.document);
	          				self.reset();
	          			};
	
	          			self.reset = function() {
		          			self.document = {
			          				name : '',
			          				tags: '',
			          				version : '',
			          				html : '',
			          				htmlWithComponent : '',
			          				description: ''
			          			};	    
	          				//$scope.register.$setPristine(); // reset Form				
	          			};

  }]);