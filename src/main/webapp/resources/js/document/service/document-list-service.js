/**
 * @author vigor8787
 */

'use strict'

App.factory('documentListService', [ '$http', '$q', function($http, $q) {

	return {

		selectDocuments : function(document) {
			return $http.post('/selectDocuments/', document).then(
				  function(response) {
					//alert('select document success');
					//console.log('creating document success');
					return response.data;
				  }, function(errResponse) {
					console.error('Error while creating document');
					return $q.reject(errResponse)
				  }
			);
		}

	};
} ]);
