require.config({
 
     paths: {
         'domtoimage': '/resources/js/dom-to-image'
     },
     
     shim: {
         'domtoimage': {
             exports: 'domtoimage'
         }
     },

     deps: ['./bootstrap']
});