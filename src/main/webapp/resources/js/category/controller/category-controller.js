/**
 * @author vigor8787
 */

'use strict'

App.controller('categoryController', [
		'$scope',
		'categoryService',

		function($scope, categoryService) {
			
			var self = this;					
						
			self.category = {
				id : '',
				name : '',
				parentid : ''
			};
			
			self.categories = [];

			self.createCategory = function(category) {
				categoryService.createCategory(category).then(
						console.log('category Creating'), 
						function(errResponse) {
							console.error('Error while creating Document');
						});
			};
}]);



