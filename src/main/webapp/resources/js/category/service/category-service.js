/**
 * @author vigor8787
 */

'use strict'

App.factory('categoryService', [ '$http', '$q', function($http, $q) {

	return {
		createCategory : function(category) {
			return $http.post('/categorySave/', category).then(
				  function(response) {
					alert('creating Category success');
					console.log('creating category success');
					return response;
				  }, function(errResponse) {
					console.error('Error while creating category');
					return $q.reject(errResponse)
				  }
			);
		},
		
		deleteCategory : function(category) {
			return $http.post('/categoryDelete/', category).then(
				  function(response) {
					alert('deleting Category success');
					console.log('deleting category success');
					return response;
				  }, function(errResponse) {
					console.error('Error while deleting category');
					return $q.reject(errResponse)
				  }
			);
		},
		
		
		selectCategories : function() {
			return $http.post('/selectCategories/').then(
				  function(response) {
					return response.data;
				  }, function(errResponse) {
					console.error('Error while listing category');
					return $q.reject(errResponse)
				  }
			);
		}

	};
} ]);
