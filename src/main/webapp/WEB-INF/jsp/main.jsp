<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>양식스토어</title>

<%@include file="../jspf/styles.jspf"%>

<style>
video#bgvid {
    position: fixed;
    top: 50%;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: -100;
    -ms-transform: translateX(-50%) translateY(-50%);
    -moz-transform: translateX(-50%) translateY(-50%);
    -webkit-transform: translateX(-50%) translateY(-50%);
    transform: translateX(-50%) translateY(-50%);
    background-size: cover;
}
</style>

</head>
<body ng-app="efialtes" class="ng-cloak">

<!--    <video autoplay loop id="bgvid" muted> <source
        src="/resources/video/main.webm" type="video/webm"></video> -->


    <div class="container">
        <%@include file="../jspf/navbar.jspf"%>
        Welcome!!
    </div>
    <!-- /container -->

    <%@include file="../jspf/js.jspf"%>

</body>
</html>