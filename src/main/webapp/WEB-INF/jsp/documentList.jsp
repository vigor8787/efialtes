<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>양식스토어</title>

<%@include file="../jspf/styles.jspf"%>

<style>
.odd {
  background: rgba(210,255,82,1) !important;
}

.even {
  background: rgba(241,231,103,1) !important;
}

.odd.selected, .even.selected {
  background: rgba(255,93,177,1) !important;
}

#listView {
            padding: 10px 5px;
            margin-bottom: -1px;
            min-height: 700px;
        }
        .document {
            float: left;
            position: relative;
            width: 190px;
            height: 350px;
            margin: 0 5px;
            padding: 0;
        }
        .document img {
            width: 190px;
            height: 250px;
        }
        .document h3 {
            margin: 0;
            padding: 3px 5px 0 0;
            max-width: 76px;
            overflow: hidden;
            line-height: 1.1em;
            font-size: .9em;
            font-weight: normal;
            text-transform: uppercase;            
            color: #999;
        }
        .document p {
            visibility: hidden;
        }
        .document:hover p {
            visibility: visible;
            position: absolute;
            width: 190px;
            height: 250px;
            top: 0;
            margin: 0;
            padding: 0;
            line-height: 200px;
            vertical-align: middle;
            text-align: center;
            color: #fff;
            background-color: rgba(0,0,0,0.75);
            transition: background .2s linear, color .2s linear;
            -moz-transition: background .2s linear, color .2s linear;
            -webkit-transition: background .2s linear, color .2s linear;
            -o-transition: background .2s linear, color .2s linear;
        }
        .k-listview:after {
            content: ".";
            display: block;
            height: 0;
            clear: both;
            visibility: hidden;
        }
</style>
</head>
<body ng-app="efialtes" class="ng-cloak">   
        
     <!-- 리스트  -->
    <div class="container">
        <%@include file="../jspf/navbar.jspf"%>

   <div ng-controller="documentListViewCtrl as listviewCtrl" ng-init="init();" class="well bs-component" style="width: 100%; height:100%;">
   
        <!-- 카테고리 Tree -->
        <div  ng-controller="documentTreeViewCtrl as dtvCtrl" class="well bs-component" style="width: 25%; height:100%; float:left">
                    
            <button type="button" class="btn btn-success btn-sm" ng-click="createNode()"><i class="glyphicon glyphicon-asterisk"></i> 카테고리 추가</button>
            <button type="button" class="btn btn-success btn-sm" ng-click="renameNode()"><i class="glyphicon glyphicon-asterisk"></i> 수정</button>
            <button type="button" class="btn btn-success btn-sm" ng-click="removeNode()"><i class="glyphicon glyphicon-asterisk"></i> 삭제</button>

            <div class="row" cg-busy="dtvCtrl.promise">
                <toaster-container></toaster-container>                
                    <div class="panel panel-default" style="OVERFLOW-Y:auto; height:700px;">
                        <div class="panel-heading"><i class="glyphicon glyphicon-tree-conifer"></i> Category</div>
                        <div class="panel-body">
                            <div  js-tree="treeConfig" should-apply="ac()" ng-model="categories"  tree="treeInstance" tree-events="ready:initNode;select_node:selectNode"></div>
                        </div>
                    </div>                
            </div>                       
        </div>
   
   
        <h4> 카테고리 Tagging</h4>
        <div ng-controller="tagController as tagCtrl">
            <tags-input ng-model="listviewCtrl.document.tags"   display-property="name"  on-tag-added="onTagUpdateForList($tag)"  on-tag-removed="onTagUpdateForList($tag)">
                <auto-complete min-length="0" source="loadTags($query)"   max-results-to-show="32"></auto-complete>
            </tags-input>
        </div>
                                        
                        
        <br><br>                                    
            <h5> 검색결과 {{documents.length}}개 항목</h5>                                               
            <button type="button" class="btn btn-info btn-sm" ng-click="createDocument()"><i class="glyphicon glyphicon-asterisk"></i> 양식 추가</button>
            <button type="button" class="btn btn-info btn-sm" ng-click="updateDocument()"><i class="glyphicon glyphicon-asterisk"></i> 수정</button>
            <button type="button" class="btn btn-info btn-sm" ng-click="removeDocument()"><i class="glyphicon glyphicon-asterisk"></i> 삭제</button>            
        
        
        
        <div class="demo-section k-content wide" style="width: 75%; height:100%; float:right">        
            <div kendo-list-view id="listView" k-data-source="source">            
                <div class="document" k-template>
                <a href ng-click="imageClick(dataItem.id)">                                        
                    <img ng-src="{{dataItem.thumbNailImage}}"   alt="test" />                                                            
                    <h3>{{ dataItem.name }}</h3>
                </a>                    
                </div>
            </div>
        
            <div kendo-pager k-data-source="source"></div>
        </div>          
            
            <script type="text/x-kendo-angular-template" id="template">
			</script>                       
    </div>                                                        
    <!-- / documentListViewCtrl ng-controller -->              
        
                                    
    </div>
    <!-- /container -->


    
    <%@include file="../jspf/js.jspf"%>


</body>
</html>