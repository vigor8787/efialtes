package com.daou.category.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class Category {

	@Id
	private String id;

	//카테고리명
	private String name;
	
	//상위 카테고리 아이디
	private String parentId;	

	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date createdDate;

	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date modifiedDate;

	public Category() {
		this.createdDate = new Date();
		this.modifiedDate = new Date();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	
}
