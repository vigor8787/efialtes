package com.daou.category.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.daou.category.model.Category;
import com.daou.category.repository.CategoryRepository;
import com.daou.document.model.Document;

@Controller
public class CategoryController {
	 
	@Autowired
	private CategoryRepository categoryRepository;
	
	@RequestMapping(value="/categorySave/", method=RequestMethod.POST)
	public ResponseEntity<Void> categorySave(@RequestBody Category category, UriComponentsBuilder ucBuilder,HttpServletRequest request) {
		
		System.out.println("Creating Category " + category.getName());		
		try {			
			categoryRepository.save(category);
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value="/categoryDelete/", method=RequestMethod.POST)
	public ResponseEntity<Void> categoryDelete(@RequestBody Category category, UriComponentsBuilder ucBuilder,HttpServletRequest request) {
		
		System.out.println("Deleting Category " + category.getName());
		try {						
			categoryRepository.remove(category); 
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	
	@RequestMapping(value="/selectCategories/", method=RequestMethod.POST)
	public ResponseEntity<List<Category>> selectCategories(UriComponentsBuilder ucBuilder) {
		
		System.out.println("select Categories ");
		List<Category> lists = new ArrayList<>();
		
		try{
			//Query query = new Query().with(new Sort(Sort.Direction., properties))
			lists = categoryRepository.findAll();
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		return new ResponseEntity<List<Category>>(lists, HttpStatus.CREATED);
	}
}
