package com.daou.category.repository;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.daou.category.model.Category;
import com.daou.document.model.Document;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository{	
	
	ApplicationContext ctx = new GenericXmlApplicationContext("SpringConfig.xml");

	MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
	
	
	@Override
	public Category save(Category category) {
		// TODO Auto-generated method stub
		mongoOperation.save(category);
		return category;
	}

	@Override
	public void remove(Category category) {
		// TODO Auto-generated method stub
		
		//양식문서 삭제
		Query query = new Query();		
		query.addCriteria(Criteria.where("categoryId").is(category.getId()));											
		mongoOperation.remove(query, Document.class);			
		
		//카테고리 삭제
		mongoOperation.remove(category);
	}

	@Override
	public List<Category> findAll() {
		// TODO Auto-generated method stub
		List<Category> lists = mongoOperation.findAll(Category.class);
		return lists;
	}

}
