package com.daou.category.repository;

import java.util.List;

import com.daou.category.model.Category;

public interface CategoryRepository {
	Category save(Category category);
	void remove (Category category);
	List<Category> findAll();
}
