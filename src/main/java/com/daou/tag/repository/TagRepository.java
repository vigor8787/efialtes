package com.daou.tag.repository;

import java.util.List;
import com.daou.tag.model.Tag;

public interface TagRepository {
	Tag save(Tag tag);
	void remove (Tag tag);
	List<Tag> findAll();
}

