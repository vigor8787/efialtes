package com.daou.tag.repository;


import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import com.daou.tag.model.Tag;

@Repository
public class TagRepositoryImpl implements TagRepository{
 
	
	ApplicationContext ctx = new GenericXmlApplicationContext("SpringConfig.xml");

	MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
	
	
	@Override
	public Tag save(Tag tag) {
		// TODO Auto-generated method stub
		mongoOperation.save(tag);
		return tag;
	}

	@Override
	public void remove(Tag tag) {
		// TODO Auto-generated method stub
		mongoOperation.remove(tag);
	}

	@Override
	public List<Tag> findAll() {
		// TODO Auto-generated method stub
		List<Tag> lists = mongoOperation.findAll(Tag.class);
		return lists;
	}
	
	
}
