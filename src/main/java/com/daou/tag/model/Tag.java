package com.daou.tag.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class Tag {

	//태그명
	@Id
	private String name;

	//전자결재 문서 키값	
	private String documentId;

	//생성일
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date createdDate;

	//수정일
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date modifiedDate;

	
	public Tag() {
		this.createdDate = new Date();
		this.modifiedDate = new Date();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	
}
