package com.daou.tag.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.daou.tag.model.Tag;
import com.daou.tag.repository.TagRepository;

@Controller
public class TagController {
	
	@Autowired
	private TagRepository tagRepository;
	
	@RequestMapping(value="/tagSave/", method=RequestMethod.POST)
	public ResponseEntity<Void> tagSave(@RequestBody Tag tag, UriComponentsBuilder ucBuilder,HttpServletRequest request) {
		
		System.out.println("Creating Tag " + tag.getName());		
		try {			
			tagRepository.save(tag);
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value="/tagDelete/", method=RequestMethod.POST)
	public ResponseEntity<Void> tagDelete(@RequestBody Tag tag, UriComponentsBuilder ucBuilder,HttpServletRequest request) {
		
		System.out.println("Deleting Tag " + tag.getName());
		try {						
			tagRepository.remove(tag); 
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	
	@RequestMapping(value="/selectTags/", method=RequestMethod.POST)
	public ResponseEntity<List<Tag>> selectTags(Tag tag, UriComponentsBuilder ucBuilder) {
		
		System.out.println("select Tags ");
		List<Tag> lists = new ArrayList<>();
		
		try{
			//Query query = new Query().with(new Sort(Sort.Direction., properties))
			lists = tagRepository.findAll();
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		return new ResponseEntity<List<Tag>>(lists, HttpStatus.CREATED);
	}
	
}
