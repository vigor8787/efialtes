package com.daou.document.repository;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.daou.document.model.Document;

@Repository
public class DocumentRepositoryImpl implements DocumentRepository{

	ApplicationContext ctx = new GenericXmlApplicationContext("SpringConfig.xml");

	MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
	
	
	@Override
	public Document save(Document document) {
		// TODO Auto-generated method stub
		mongoOperation.save(document);
		return document;
	}

	@Override
	public void remove(Document document) {
		// TODO Auto-generated method stub
		mongoOperation.remove(document);
	}

	@Override
	public List<Document> findAll() {
		return mongoOperation.findAll(Document.class);			
	}
	
	@Override
	public List<Document> findAll(Document document) {
		//BasicQuery query = new BasicQuery("{categoryId : 'cat' }");				
		List<String> tags = document.getTags();
		String categoryId = document.getCategoryId();
		
		Query query = new Query();
		
		if(!"root".equals(categoryId)) {
			query.addCriteria(Criteria.where("categoryId").is(document.getCategoryId()));	
		}
								
		if(tags.size() > 0) {
			query.addCriteria(Criteria.where("tags").in(tags));
		}
		
		return mongoOperation.find(query, Document.class);			
	}

}
