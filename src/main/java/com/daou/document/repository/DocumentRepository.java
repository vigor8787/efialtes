package com.daou.document.repository;

import java.util.List;

import com.daou.document.model.Document;

public interface DocumentRepository {
	Document save(Document document);
	void remove (Document document);
	List<Document> findAll();
	List<Document> findAll(Document document);
}
