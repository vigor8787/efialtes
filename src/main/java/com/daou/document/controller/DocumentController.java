package com.daou.document.controller;

import java.awt.ComponentOrientation;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.daou.document.model.Document;
import com.daou.document.repository.DocumentRepositoryImpl;

import gui.ava.html.image.generator.HtmlImageGenerator;

@Controller
public class DocumentController {
	
	@Autowired
	private DocumentRepositoryImpl documentRepository;
	
	@RequestMapping("/documentRegister")
	public String goDocumentRegister() {
		return "documentRegister";
	}
	
	
	@RequestMapping("/documentList")
	public String goDocumentList() {
		return "documentList";
	}
	
	@RequestMapping(value="/getImage", method=RequestMethod.GET)
	public void getImage (String path, HttpServletResponse re) throws IOException {
		OutputStream out = null;
		InputStream in = null;
		try {
			out = re.getOutputStream();			
			in = new FileInputStream(path);
			IOUtils.copy(in,out);
			
		} finally {
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}
		System.out.println(path);
	}
	
	@RequestMapping(value="/documentSave/", method=RequestMethod.POST)
	public ResponseEntity<Void> saveDocument(@RequestBody Document document, UriComponentsBuilder ucBuilder,HttpServletRequest request) {
		
		System.out.println("Creating Document " + document.getName());
		
		try {
			//썸네일 이미지 생성
			String fileFullPath = convertHtmlToImage(document.getHtml(),document.getName(), document.getId());			
			
			document.setThumbNailImage(fileFullPath);
			
			documentRepository.save(document);
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/selectDocuments/", method=RequestMethod.POST)
	public ResponseEntity<List<Document>> selectDocuments(@RequestBody Document document, UriComponentsBuilder ucBuilder) {
		
		System.out.println("select Documents ");
		
		List<Document> lists = new ArrayList<>();
		
		String categoryId = document.getCategoryId();
		List<String> tags = document.getTags();
		
		try{
			//documentRepository.save(document);
			if(!categoryId.isEmpty()) {
				if("root".equals(categoryId) && tags.size() <= 0) {
					lists = documentRepository.findAll();
				}else {
					lists = documentRepository.findAll(document);
				}
			}
		
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
		//HttpHeaders headers = new HttpHeaders();
		//return new ResponseEntity<List<Document>>(headers, HttpStatus.CREATED);
		return new ResponseEntity<List<Document>>(lists, HttpStatus.CREATED);
	}

	
	/**
	 * Convert Html to Image save
	 * */
	public String convertHtmlToImage(String html, String name, String id) {
		 
		//Generator 생성
		HtmlImageGenerator htmlImageGenerator = new HtmlImageGenerator();
		htmlImageGenerator.loadHtml(html);			
		//String fileFullPath = contextPath + name+".png";
		long currentTime = System.currentTimeMillis();
		
		String fileFullPath = "/image/" + name+ '_' + String.valueOf(currentTime)+".png";
		File file = new File(fileFullPath);
		htmlImageGenerator.saveAsImage(file); 		
		
		return fileFullPath;
	}
	
	
	/**
	 * API Call (결재양식 All Select)
	 * */
	@RequestMapping(value="/api/selectDocuments", method=RequestMethod.GET)
	public ResponseEntity<List<Document>> apiSelectDocuments() {
		
		List<Document> lists = new ArrayList<>();
		
		try{
			//documentRepository.save(document);
			lists = documentRepository.findAll();
		
		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		
		return new ResponseEntity<List<Document>>(lists, HttpStatus.CREATED);
	}
	
}
