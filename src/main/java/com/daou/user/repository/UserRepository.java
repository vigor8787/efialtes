package com.daou.user.repository;

import java.util.List;

import com.daou.user.model.User;

public interface UserRepository {

	User save(User user);
	void remove (User user);
	List<User> findAll();
}
